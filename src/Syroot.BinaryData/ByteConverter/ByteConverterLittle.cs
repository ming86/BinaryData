﻿using System;
using System.Security;

namespace Syroot.BinaryData
{
    /// <summary>
    /// Represents a <see cref="ByteConverter"/> which handles little endianness.
    /// </summary>
    [SecuritySafeCritical]
    public sealed class ByteConverterLittle : ByteConverter
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="BinaryData.Endian"/> in which data is stored as converted by this instance.
        /// </summary>
        public override Endian Endian => Endian.Little;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Stores the specified <see cref="Double"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        [SecuritySafeCritical]
        public override unsafe void GetBytes(Double value, Span<byte> buffer)
        {
            UInt64 raw = *(UInt64*)&value;
            buffer[0] = (byte)raw;
            buffer[1] = (byte)(raw >> 8);
            buffer[2] = (byte)(raw >> 16);
            buffer[3] = (byte)(raw >> 24);
            buffer[4] = (byte)(raw >> 32);
            buffer[5] = (byte)(raw >> 40);
            buffer[6] = (byte)(raw >> 48);
            buffer[7] = (byte)(raw >> 56);
        }

        /// <summary>
        /// Stores the specified <see cref="Int16"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        public override void GetBytes(Int16 value, Span<byte> buffer)
        {
            buffer[0] = (byte)value;
            buffer[1] = (byte)(value >> 8);
        }

        /// <summary>
        /// Stores the specified <see cref="Int32"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        public override void GetBytes(Int32 value, Span<byte> buffer)
        {
            buffer[0] = (byte)value;
            buffer[1] = (byte)(value >> 8);
            buffer[2] = (byte)(value >> 16);
            buffer[3] = (byte)(value >> 24);
        }

        /// <summary>
        /// Stores the specified <see cref="Int64"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        public override void GetBytes(Int64 value, Span<byte> buffer)
        {
            buffer[0] = (byte)value;
            buffer[1] = (byte)(value >> 8);
            buffer[2] = (byte)(value >> 16);
            buffer[3] = (byte)(value >> 24);
            buffer[4] = (byte)(value >> 32);
            buffer[5] = (byte)(value >> 40);
            buffer[6] = (byte)(value >> 48);
            buffer[7] = (byte)(value >> 56);
        }

        /// <summary>
        /// Stores the specified <see cref="Single"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        [SecuritySafeCritical]
        public override unsafe void GetBytes(Single value, Span<byte> buffer)
        {
            UInt32 raw = *(UInt32*)&value;
            buffer[0] = (byte)raw;
            buffer[1] = (byte)(raw >> 8);
            buffer[2] = (byte)(raw >> 16);
            buffer[3] = (byte)(raw >> 24);
        }

        /// <summary>
        /// Stores the specified <see cref="UInt16"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        public override void GetBytes(UInt16 value, Span<byte> buffer)
        {
            buffer[0] = (byte)value;
            buffer[1] = (byte)(value >> 8);
        }

        /// <summary>
        /// Stores the specified <see cref="UInt32"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        public override void GetBytes(UInt32 value, Span<byte> buffer)
        {
            buffer[0] = (byte)value;
            buffer[1] = (byte)(value >> 8);
            buffer[2] = (byte)(value >> 16);
            buffer[3] = (byte)(value >> 24);
        }

        /// <summary>
        /// Stores the specified <see cref="UInt64"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        public override void GetBytes(UInt64 value, Span<byte> buffer)
        {
            buffer[0] = (byte)value;
            buffer[1] = (byte)(value >> 8);
            buffer[2] = (byte)(value >> 16);
            buffer[3] = (byte)(value >> 24);
            buffer[4] = (byte)(value >> 32);
            buffer[5] = (byte)(value >> 40);
            buffer[6] = (byte)(value >> 48);
            buffer[7] = (byte)(value >> 56);
        }

        /// <summary>
        /// Returns an <see cref="Double"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        [SecuritySafeCritical]
        public override unsafe Double ToDouble(ReadOnlySpan<byte> buffer)
        {
            Int64 raw = buffer[0]
                | (long)buffer[1] << 8
                | (long)buffer[2] << 16
                | (long)buffer[3] << 24
                | (long)buffer[4] << 32
                | (long)buffer[5] << 40
                | (long)buffer[6] << 48
                | (long)buffer[7] << 56;
            return *(Double*)&raw;
        }

        /// <summary>
        /// Returns an <see cref="Int16"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override Int16 ToInt16(ReadOnlySpan<byte> buffer)
        {
            return (Int16)(buffer[0]
                | buffer[1] << 8);
        }

        /// <summary>
        /// Returns an <see cref="Int32"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override Int32 ToInt32(ReadOnlySpan<byte> buffer)
        {
            return buffer[0]
                | buffer[1] << 8
                | buffer[2] << 16
                | buffer[3] << 24;
        }

        /// <summary>
        /// Returns an <see cref="Int64"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override Int64 ToInt64(ReadOnlySpan<byte> buffer)
        {
            return buffer[0]
                | (long)buffer[1] << 8
                | (long)buffer[2] << 16
                | (long)buffer[3] << 24
                | (long)buffer[4] << 32
                | (long)buffer[5] << 40
                | (long)buffer[6] << 48
                | (long)buffer[7] << 56;
        }

        /// <summary>
        /// Returns an <see cref="Single"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        [SecuritySafeCritical]
        public override unsafe Single ToSingle(ReadOnlySpan<byte> buffer)
        {
            Int32 raw = buffer[0]
                | buffer[1] << 8
                | buffer[2] << 16
                | buffer[3] << 24;
            return *(Single*)&raw;
        }

        /// <summary>
        /// Returns an <see cref="UInt16"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override UInt16 ToUInt16(ReadOnlySpan<byte> buffer)
        {
            return (UInt16)(buffer[0]
                | buffer[1] << 8);
        }

        /// <summary>
        /// Returns an <see cref="UInt32"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override UInt32 ToUInt32(ReadOnlySpan<byte> buffer)
        {
            return (UInt32)(buffer[0]
                | buffer[1] << 8
                | buffer[2] << 16
                | buffer[3] << 24);
        }

        /// <summary>
        /// Returns an <see cref="UInt64"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override UInt64 ToUInt64(ReadOnlySpan<byte> buffer)
        {
            return buffer[0]
                | (ulong)buffer[1] << 8
                | (ulong)buffer[2] << 16
                | (ulong)buffer[3] << 24
                | (ulong)buffer[4] << 32
                | (ulong)buffer[5] << 40
                | (ulong)buffer[6] << 48
                | (ulong)buffer[7] << 56;
        }
    }
}
