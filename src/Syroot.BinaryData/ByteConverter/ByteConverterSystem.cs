﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Syroot.BinaryData
{
    /// <summary>
    /// Represents a <see cref="ByteConverter"/> which handles system endianness.
    /// </summary>
    [SecuritySafeCritical]
    public sealed class ByteConverterSystem : ByteConverter
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly Endian _systemEndianness = BitConverter.IsLittleEndian ? Endian.Little : Endian.Big;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="BinaryData.Endian"/> in which data is stored as converted by this instance.
        /// </summary>
        public override Endian Endian => _systemEndianness;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Stores the specified <see cref="Double"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        [SecuritySafeCritical]
        public override unsafe void GetBytes(Double value, Span<byte> buffer)
            => new Span<byte>(&value, sizeof(Double)).CopyTo(buffer);

        /// <summary>
        /// Stores the specified <see cref="Int16"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        [SecuritySafeCritical]
        public override unsafe void GetBytes(Int16 value, Span<byte> buffer)
            => new Span<byte>(&value, sizeof(Int16)).CopyTo(buffer);

        /// <summary>
        /// Stores the specified <see cref="Int32"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        [SecuritySafeCritical]
        public override unsafe void GetBytes(Int32 value, Span<byte> buffer)
            => new Span<byte>(&value, sizeof(Int32)).CopyTo(buffer);

        /// <summary>
        /// Stores the specified <see cref="Int64"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        [SecuritySafeCritical]
        public override unsafe void GetBytes(Int64 value, Span<byte> buffer)
            => new Span<byte>(&value, sizeof(Int64)).CopyTo(buffer);

        /// <summary>
        /// Stores the specified <see cref="Single"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        [SecuritySafeCritical]
        public override unsafe void GetBytes(Single value, Span<byte> buffer)
            => new Span<byte>(&value, sizeof(Single)).CopyTo(buffer);

        /// <summary>
        /// Stores the specified <see cref="UInt16"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        [SecuritySafeCritical]
        public override unsafe void GetBytes(UInt16 value, Span<byte> buffer)
            => new Span<byte>(&value, sizeof(UInt16)).CopyTo(buffer);

        /// <summary>
        /// Stores the specified <see cref="UInt32"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        [SecuritySafeCritical]
        public override unsafe void GetBytes(UInt32 value, Span<byte> buffer)
            => new Span<byte>(&value, sizeof(UInt32)).CopyTo(buffer);

        /// <summary>
        /// Stores the specified <see cref="UInt64"/> value as bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="buffer">The byte array to store the value in.</param>
        [SecuritySafeCritical]
        public override unsafe void GetBytes(UInt64 value, Span<byte> buffer)
            => new Span<byte>(&value, sizeof(UInt64)).CopyTo(buffer);

        /// <summary>
        /// Returns an <see cref="Double"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override Double ToDouble(ReadOnlySpan<byte> buffer) => MemoryMarshal.Cast<byte, Double>(buffer)[0];

        /// <summary>
        /// Returns an <see cref="Int16"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override Int16 ToInt16(ReadOnlySpan<byte> buffer) => MemoryMarshal.Cast<byte, Int16>(buffer)[0];

        /// <summary>
        /// Returns an <see cref="Int32"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override Int32 ToInt32(ReadOnlySpan<byte> buffer) => MemoryMarshal.Cast<byte, Int32>(buffer)[0];

        /// <summary>
        /// Returns an <see cref="Int64"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override Int64 ToInt64(ReadOnlySpan<byte> buffer) => MemoryMarshal.Cast<byte, Int64>(buffer)[0];

        /// <summary>
        /// Returns an <see cref="Single"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override Single ToSingle(ReadOnlySpan<byte> buffer) => MemoryMarshal.Cast<byte, Single>(buffer)[0];

        /// <summary>
        /// Returns an <see cref="UInt16"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override UInt16 ToUInt16(ReadOnlySpan<byte> buffer) => MemoryMarshal.Cast<byte, UInt16>(buffer)[0];

        /// <summary>
        /// Returns an <see cref="UInt32"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override UInt32 ToUInt32(ReadOnlySpan<byte> buffer) => MemoryMarshal.Cast<byte, UInt32>(buffer)[0];

        /// <summary>
        /// Returns an <see cref="UInt64"/> instance converted from the bytes in the given <paramref name="buffer"/>.
        /// </summary>
        /// <param name="buffer">The byte array storing the raw data.</param>
        /// <returns>The converted value.</returns>
        public override UInt64 ToUInt64(ReadOnlySpan<byte> buffer) => MemoryMarshal.Cast<byte, UInt64>(buffer)[0];
    }
}
