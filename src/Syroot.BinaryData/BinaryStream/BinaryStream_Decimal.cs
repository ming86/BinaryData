﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        // ---- Read 1 Decimal ----

        /// <summary>
        /// Returns a <see cref="Decimal"/> instance read from the underlying stream.
        /// </summary>
        /// <returns>The value read from the current stream.</returns>
        public Decimal ReadDecimal()
            => BaseStream.ReadDecimal();

        /// <summary>
        /// Returns a <see cref="Decimal"/> instance read asynchronously from the underlying stream.
        /// </summary>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>The value read from the current stream.</returns>
        public async Task<Decimal> ReadDecimalAsync(
            CancellationToken cancellationToken = default)
            => await BaseStream.ReadDecimalAsync(cancellationToken);

        // ---- Read N Decimals ----

        /// <summary>
        /// Returns an array of <see cref="Decimal"/> instances read from the underlying stream.
        /// </summary>
        /// <param name="count">The number of values to read.</param>
        /// <returns>The array of values read from the current stream.</returns>
        public Decimal[] ReadDecimals(int count)
            => BaseStream.ReadDecimals(count);

        /// <summary>
        /// Returns an array of <see cref="Decimal"/> instances read asynchronously from the underlying stream.
        /// </summary>
        /// <param name="count">The number of values to read.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>The array of values read from the current stream.</returns>
        public async Task<Decimal[]> ReadDecimalsAsync(int count,
            CancellationToken cancellationToken = default)
            => await BaseStream.ReadDecimalsAsync(count, cancellationToken);

        // ---- Write 1 Decimal ----

        /// <summary>
        /// Writes a <see cref="Decimal"/> value to the underlying stream.
        /// </summary>
        /// <param name="value">The value to write.</param>
        public void Write(Decimal value)
            => BaseStream.Write(value);

        /// <summary>
        /// Writes a <see cref="Decimal"/> value asynchronously to the underlying stream.
        /// </summary>
        /// <param name="value">The value to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteAsync(Decimal value,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(value, cancellationToken);

        /// <summary>
        /// Writes a <see cref="Decimal"/> value to the underlying stream.
        /// </summary>
        /// <param name="value">The value to write.</param>
        public void WriteDecimal(Decimal value)
            => BaseStream.Write(value);

        /// <summary>
        /// Writes a <see cref="Decimal"/> value asynchronously to the underlying stream.
        /// </summary>
        /// <param name="value">The value to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteDecimalAsync(Decimal value,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(value, cancellationToken);

        // ---- Write N Decimals ----

        /// <summary>
        /// Writes an enumerable of <see cref="Decimal"/> values to the underlying stream.
        /// </summary>
        /// <param name="values">The values to write.</param>
        public void Write(IEnumerable<Decimal> values)
            => BaseStream.Write(values);

        /// <summary>
        /// Writes an enumerable of <see cref="Decimal"/> values asynchronously to the underlying stream.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteAsync(IEnumerable<Decimal> values,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(values, cancellationToken);

        /// <summary>
        /// Writes an enumerable of <see cref="Decimal"/> values to the underlying stream.
        /// </summary>
        /// <param name="values">The values to write.</param>
        public void WriteDecimals(IEnumerable<Decimal> values)
            => BaseStream.WriteDecimals(values);

        /// <summary>
        /// Writes an enumerable of <see cref="Decimal"/> values asynchronously to the underlying stream.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteDecimalsAsync(IEnumerable<Decimal> values,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(values, cancellationToken);
    }
}
